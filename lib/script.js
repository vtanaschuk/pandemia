const pandemicStartMap = "0X10010X000X010X0";
let map = pandemicStartMap.split("X");
let ln = pandemicStartMap.length;
function countAllPeople(pandemicStartMap) {
    let j = 0;
    for(let i = 0; i < ln; i++){
        if(pandemicStartMap[i]==='0'||pandemicStartMap[i]==='1'){
            j = j+1;
        }
    }
    return j;
}
let total = countAllPeople(pandemicStartMap);
function detectIllPeople(map){
    let pandemicEndMapArr = [];
    let illregion ='';
    for(let k=0; k<map.length; k++){
        let island ="";
        for(let z = 0; z < map[k].length; z++){
            // console.log(map[k].length);
            if(map[k][z]==="1"){
                illregion += map[k];
                for (let q = 0; q < map[k].length; q++){
                    island += "1";
                }
                pandemicEndMapArr.push(island);
                break;
            }else if(map[k][z]==="0" && z >= map[k].length-1){
                pandemicEndMapArr.push(map[k]);
                break;
            }
        }
    }
    let pandemicEndMap = pandemicEndMapArr.join('X');
    return [illregion, pandemicEndMap];
}

let detect = detectIllPeople(map);
// console.log(detect[0]+' '+detect[1]);
let illPeople = detect[0];
console.log(illPeople);
let infected = illPeople.length;
// console.log(infected+' ill people, '+ total + ' all people');
let percentage = infected / total * 100;
// console.log(percentage+' %');

window.onload = function() {
    function showPandemicFunction(pandemicStartMap, selector){
        for (let i = 0; i<pandemicStartMap.length; i++){
            if (pandemicStartMap[i]==="X"){
                document.querySelector(selector).innerHTML +="<div class='box sea'></div>";
            }else if(pandemicStartMap[i]==="0"){
                document.querySelector(selector).innerHTML +="<div class='box healthy'></div>";
            }else{
                document.querySelector(selector).innerHTML +="<div class='box ill'></div>";
            }
        }
        return false;
    }
    showPandemicFunction(pandemicStartMap,'.start');
    showPandemicFunction(detect[1],'.end');

    document.querySelector('.infected').innerHTML +='total: '+ total + " infected: "+ infected;
    document.querySelector('.percentage').innerHTML +='percentage: '+ percentage+"%";
}

